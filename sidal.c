#include <libgen.h>
#include "utils.h"

#define START 1<<0
#define STOP 1<<1
#define AVAIL 1<<2
#define UNAVAIL 1<<3
#define WAIT 1<<4
#define MARK 1<<5
#define CHECK 1<<6

static int doservice(const char *path);
static char* findpath(char *name);
static int runservice(char *target);
static void usage(char *name);

static short int mode=0;
static char *ecmd=NULL; /* overwritten cmd */

int
doservice(const char *path)
{
	int ret=1;
	pid_t service;
	char **cmd=(char **)malloc(3*sizeof(char*));
	cmd[0]=path;
	cmd[2]=NULL;
	if (ecmd) {
		cmd[1]=ecmd;
	} else if ((mode & START) && (mode & STOP)) {
		cmd[1]="restart";
	} else if (mode & START) {
		cmd[1]="start";
	} else if (mode & STOP) {
		cmd[1]="stop";
	} else if (mode & CHECK) {
		cmd[1]="check";
	} else {
		cmd[1]=NULL;
	}
	service=spawn(cmd);
	free(cmd);
	if (mode & WAIT) {
		/* wstatus should be initialized to trace errors */
		int wstatus=-1;
		waitpid(service,&wstatus,0);
		ret=WIFEXITED(wstatus);
		if (ret) {
			if (WEXITSTATUS(wstatus) != 0) {
				ret=0;
			}
		}
	}
	return ret;
}

char*
findpath(char *name)
{
	char **paths;
	FILE *fp;
	if (mode & STOP || mode & CHECK) {
		paths=(char**)malloc(4*sizeof(char*));
		paths[0]="/run/sidal/";
		paths[1]="/etc/sidal/run/";
		paths[2]="/etc/sidal/avail/";
		paths[3]=NULL;
	} else if (mode & START) {
		paths=(char**)malloc(11*sizeof(char*));
		paths[0]="/etc/sidal/run/";
		paths[1]="/etc/sidal/avail/";
		paths[2]="/bin/";
		paths[3]="/sbin/";
		paths[4]="/usr/bin/";
		paths[5]="/usr/sbin/";
		paths[6]="/usr/local/bin/";
		paths[7]="/usr/local/sbin/";
		paths[8]="";
		paths[9]="./";
		paths[10]=NULL;
	}
	else if (mode & AVAIL) {
		paths=(char**)malloc(2*sizeof(char*));
		paths[0]="/etc/sidal/avail/";
		paths[1]=NULL;
	}
	else if (mode & UNAVAIL) {
		paths=(char**)malloc(2*sizeof(char*));
		paths[0]="/etc/sidal/run/";
		paths[1]=NULL;
	}
	for (int i=0;paths[i];i+=1) {
		if ((fp=fopen(smprintf("%s%s",paths[i],name),"r"))) {
			const char *path=paths[i];
			free(paths);
			fclose(fp);
			return smprintf("%s%s",path,name);
		}
	}
	return 0;
}

int
runservice(char *target)
{
	int res;
	char *path=findpath(target);
	if (!path) {
		warn("cannot find service: %s",target);
		return -1;
	}
	if ((mode&START) | (mode&STOP)) {
		printf("%s service: %s\n",((mode & START) && (mode & STOP)) ? "restarting" : (mode & START) ? "starting" : "stopping",target);
		res=doservice(path);
		if (mode&WAIT) {
			if (res) {
				printf("service %s successfully: %s\n",((mode & START) && (mode & STOP)) ? "restarted" : (mode & START) ? "started" : "stopped",target);
			} else {
				printf("errors occured while %s service: %s\n",((mode & START) && (mode & STOP)) ? "restarting" : (mode & START) ? "starting" : "stopping",target);
			}
		}
		if (mode&MARK && res) {
			if ((mode & STOP) && strstr(path,"/run/sidal/")) {
				system(smprintf("rm %s",path));
			}
			if (mode & START) {
				system(smprintf("ln -sf %s /run/sidal",path));
			}
		}
	} else if (mode & CHECK) {
		mode=mode|WAIT;
		res=doservice(path);
		if (res) {
			printf("service %s is running\n", target);
		} else {
			printf("service %s is not running\n", target);
		}
	} else if (mode & AVAIL) {
		system(smprintf("ln -sf %s /etc/sidal/run",path));
	} else if (mode & UNAVAIL) {
		system(smprintf("rm %s",path));
	}
	free(path);
	return 0;
}

void
usage(char *name)
{
	printf("usage: %s [acdefklmprsuw] [cmd] services\n",name);
	exit(0);
}

int
main(int argc, char *argv[])
{
	int i, nsvc;
	FILE *ex;
	
	if (argc<2)
		usage(argv[0]);
	if (strchr(argv[1],'d')) {
		system("mkdir /etc/sidal");
		system("mkdir /etc/sidal/avail");
		system("mkdir /etc/sidal/default");
		system("mkdir /etc/sidal/run");
		system("mkdir /run/sidal");
		exit(0);
	}
	if (strchr(argv[1],'l')) {
		system("ls /etc/sidal/run");
		exit(0);
	}
	if (strchr(argv[1],'a')) {
		mode=mode|AVAIL;
	} else if (strchr(argv[1],'u')) {
		mode=mode|UNAVAIL;
	} else if (strchr(argv[1],'s')) {
		mode=mode|START;
	} else if (strchr(argv[1],'k')) {
		mode=mode|STOP;
	} else if (strchr(argv[1],'r')) {
		mode=mode|START|STOP;
	} else if (strchr(argv[1],'c')) {
		mode=mode|CHECK;
	}

	if (strchr(argv[1],'w')) {
		mode=mode|WAIT;
	}
	if (strchr(argv[1],'m')) {
		mode=mode|MARK;
	}
	if (strchr(argv[1],'e')){
		ecmd=argv[2];
	}

	nsvc=argc-(ecmd ? 3 : 2);

	if (!nsvc) {
		if (mode&AVAIL) {
			system("ls /etc/sidal/avail");
		} else if (mode & START) {
			system("ls /run/sidal");
		} else {
			usage(argv[0]);
		}
	}
	if (!strchr(argv[1],'p') || nsvc==1) {
		if (!strchr(argv[1],'f')) {
			for (i=0;i<nsvc;i+=1) {
				runservice(argv[2+(ecmd ? 1 : 0)+i]);
			}
		}
		else {
			for (i=0;i<nsvc;i+=1) {
				if ((ex=fopen(smprintf("/run/sidal/%s",basename(argv[2+(ecmd ? 1 : 0)+i])),"r")) && mode&START && !(mode&STOP)) {
					fclose(ex);
				} else if (!(ex=fopen(smprintf("/run/sidal/%s",basename(argv[2+(ecmd ? 1 : 0)+i])),"r")) && !(mode&START) && mode&STOP);
				else runservice(argv[2+(ecmd ? 1 : 0)+i]);
			}
		}
	} else {
		char **cmd=(char**)malloc(sizeof(char*)*5);
		cmd[0]=argv[0];
		cmd[1]=argv[1];
		cmd[3]=NULL;
		cmd[4]=NULL;
		if (ecmd) {
			cmd[2]=argv[2];
		}
		for (i=0; i<nsvc; i+=1) {
			cmd[2+(ecmd ? 1 : 0)]=argv[2+(ecmd ? 1 : 0)+i];
			spawn(cmd);
		}
		for (i=0; i<nsvc; i+=1)
			waitpid(-1, NULL, 0);
		free(cmd);
	}

	return 0;
}
